package main

import (
	"fmt"
	"time"

	"gitee.com/vnaki/gt"
)

type Model struct {
	Id        int32     `db:"id,omitempty" gen:"pk,ai"`
	CreatedAt time.Time `db:"created_at"`
	SDK       bool      `db:"sdk" gen:"type:tinyint,length:1"`
}

type ThreeStudentModel struct {
	Model
	Num     uint64  `db:"num" gen:"notnull,default:0,comment:数字"`
	Name    string  `db:"name" gen:"notnull,default:,comment:名称"`
	Content string  `db:"content" gen:"type:text"`
	Score   float32 `db:"score" gen:"length:1,decimal:1,default:1,notnull,unsigned"`
	Money   float64 `db:"money" gen:"length:10,decimal:2,default:1,notnull,unsigned,comment:余额"`
}

type TwoStudent struct {
	Model
	Name    string `db:"name" gen:"notnull"`
	Content string `db:"content"`
	Score   int    `db:"score" gen:"length:1,decimal:1,default:1,notnull,unsigned"`
}

// gen: length:1,decimal:2,default:111,pk,ai,unsigned,notnull

// int int8 int16 int32 int64 byte rune
// uint uint8 uint16 uint32 uint64 byte rune
// float32 float64
// char varchar text
// datetime

// TINYINT	-128〜127	0 〜255        int8
// SMALLINT	-32768〜32767	0〜65535   int16
// MEDIUMINT	-8388608〜8388607	0〜16777215
// INT (INTEGER)	-2147483648〜2147483647	0〜4294967295   int32
// BIGINT	-9223372036854775808〜9223372036854775807	0〜18446744073709551615 int64 int
//

func main() {
	b := gt.New()
	b.SetSchema("stu")
	b.SetDrop(true)
	b.SetWrap(true)
	sql, err := b.Model(ThreeStudentModel{})
	fmt.Println(sql, err)

	sql, err = b.Model(TwoStudent{}, "twostu")
	fmt.Println(sql, err)

	b = gt.New()
	b.SetDrop(true)
	b.SetWrap(true)
	b.SetMode(gt.MYSQL)

	sql, err = b.Model(ThreeStudentModel{})
	fmt.Println(sql, err)
	sql, err = b.Model(TwoStudent{}, "twostu")
	fmt.Println(sql, err)
}
